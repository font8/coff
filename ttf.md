# TrueType

## as a target

The, now ancient, `ttf8` tool can produce TrueType `glyf`
outlines.
In 2022 this only works for linear segments.

The extent to which `ttf8` uses coff is limited.
It uses a Glyph model which is closer to TTF than coff;
it would be good to add a more abstract glyph model to coff,
and have `ttf8` delay conversion from coff to TTF.

Supporting quadratics involes two parts:

- coff to coff gridding, which at a minimum converts to integer
  grid, but also should make some decisions about implicit
  points;
- conversion to TTF model and compilation to `glyf`
  representation.

The compilation to `glyf` byte is in principle implemented.
Existing code (CompileGlyph, Flags, XY, and so on) is crude, but
does honour the on-curve flags.
Curves are unlikely to have been tested however.


## from what source?

TTF as a target isn't that interesting until we have SVG, say,
as a possible source.
That involves extending the code in `pathdata` and in `ttf8` to
handle `Q` commands.


## Gridding for TrueType

A crude version of this would be to round each point to the
integer grid.
In this crude version implicit on-curve points are an
optimisation that doesn't change the geometry.

However, observe that TrueType's implicit on-curve point rule
allows such implicit points to have half-integer coordinates.
I have not yet tested if renderers model this.

Exploiting the possibility of half-integer coordinates for
implicit points requires doing implicit points at the same time
as gridding.

Something like:

- put all off-curve points on the integer grid
- for each on-curve point compute both its nearest integer grid
  point and the implicit point that is the average of its
  (now-gridded) neighbours
- use whichever of those two is nearest the true on-curve point

Note that the way the algorithm is written here involves
computing the implicit point even if it is very different from
the on-curve point and would never be used.

# END
