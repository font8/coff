# CFF—Compact Font Format

This document discusses primarily the Type 2 Charstrings that
are used in CFF fonts to describes outlines.

The glyph outlines are described using Type 2 Charstrings.
Geometrically they consist of a sequence of linked cubic Beziérs
and lines, but the Charstring description is essentially
semi-computational.
It includes arithmetic operators, subroutines, and
an `ifelse` conditional.
Finite stack and array storage and a lack of any loop,
tail-call, or goto means that it is not (deliberately) Turing.

In the more recent `CFF2` variant, the computation operators
have been removed, leaving only geometry and subrs.


## target CFF

As a target a crude compiler would be relatively simple.
Components would be flattened or represented as subrs.
Contours map relatively straightforwardly to the line or
curve motions of CFF.

Gridding would be done to an integer grid; though
CFF supports fractional coordinates (see section below),
tool and rendered support is limited.

It would be worth implementing a control for gridding
so that i can research support for fractional coordinates more
easily.


## CFF source

To some extent this is already implemented in `glox`.
Subroutines are not implemented.


## Components

CFF doesn't have components in the TrueType sense but
it does have subroutines.
But because of the computational nature of the descriptions,
a subroutine need not encode what we would think of as a component.
They are used for components (including the simplest case, where
one glyph is identical to another such as Latin X and Cyrillic Ha
(this example taken from Ouroboros)), but
also for things like setting up stem hints, and
parts of outlines that are smaller than components
(like terminals and serifs).

TrueType components can be geometrically transformed, but
CFF has no direct equivalent.
But subroutines are capable enough that it would be possible to
do this (you have to write the transformation using `mul` and `add`).
No idea if this is done in practice.
And it wouldn't work in CFF2 anyway.

A Glyphs.app font that has no components appears not to use any
subrs in the CFF output.
Although this probably depends on being in _release mode_ (see
discussion below).


## Subroutines

Mostly covered in the Components discussion.

Tools that make final production `.otf` file using AFDKO `makeotf` 
(probably most of them in 2022) probably use it to control
subroutinisation.
`makeotf` has the `-S` option to switch subroutinisation on (`-nS`
to switch off).
It produces subroutines that are very "unnatural",
they are optimisation artefacts, not natural graphic components.

The subroutinisation option is part of the umbrella release option.

`makeotf -nS` (the default) can be used to "unsubroutinise" a
font, which flattens all the geometry and removes all subrs.

There are some fonts that appear to implement "natural"
subroutines.
One such is Goudy Bookletter 1911 (The League of Moveable Type)
which was created using FontForge, so possibly that is a
FontForge artefact.


## Coordinates

As fas as i can tell, fractional (32-bit fixed point 16.16)
numbers are a thing and you can use them as coordinates.

I don't think this is very well used, but
whatever hinted Ouroboros uses half-grid coordinates, N.5, in stem hints
(occasionally, but enough to be easy to find).
For example see `uni2196`.

A brief experiment (in 2022-04) with `ttx` suggests that CFF can
represent fractional coordinates and `makeotf` preserves them
when doing OTF to OTF conversion.

`glox` can convert CFF with fractional coordinates (to SVG).
`hb-view` however appears to render them incorrectly.
Apple's Font Book preview appears to render them correctly
(though i did not install the font).


## Hints

Hints are a significant part of CFF, at least when you look at
the Charstrings of compiled font files.

In CFF a hint is either for a vertical stem or a horizontal stem
or a counter.
A stem hint describes an infinitely long but finitely thick
rectangle parallel to either horizontal axis (`hstem`) or vertical
axis (`vstem`).

Counter hints are described by the stem hints that they are
bounded by (i think), they are intended for rectangular counters
in Kanji and similar writing systems.

Active (stem) hints must not overlap and there is a mask
mechanism to enable and disable any set of hints,
at any time in the execution of the Charstring.

# END
