# Comments on UFO

UFO is one of the more interesting targets for COFF / Glyph Exchange,
because it appears to be the most useful editable interchange format.

Note that UFO attempts to represent an entire font
(and more implicitly, font families), but COFF is less ambitious
(so far).

This document is a sketch analysis of what would be required,
in terms of what data comes from where,
to convert from OTF to UFO, primarily considering the glyphs.

For COFF, UFO serves as a Nearest Natural Model.
Features that are in UFO but not in COFF are problematic, and
possibly prioritised for modelling in COFF.
Similarly features in COFF that are not in UFO are unlikely to
gain traction in the wider font-making community.

Because of this closeness between COFF and UFO it is worth
using the same terminology and models and datatypes where
possible (and where not, keeping them as coherent as possible).

Note the primary glyph representation:

A _glyph_ has some metadata and for its graphica data, _an outline_;
An _outline_ is described by _contours_
(and possibly _components_).

An _outline_ has no attributes (they are better associated with
the glyph), but a _contour_ can had a identifier (and _points_
in contours can have names and identifiers).


## File organisation

A UFO is an organised series of directories and files with a
single top-level directory, or a zip of the same.
Most of the data is stored in XML files.

See the spec for details, this is an extremely minimal list of
notes.

- afont.ufo/layercontents.plist # create a placeholder for now
- afont.ufo/metainfo.plist # create a placeholder for now
- afont.ufo/glyphs/contents.plist # created


## Bulk glyphs

In UFO glyph data is stored in `.glif` files, one per glyph.

See https://unifiedfontobject.org/versions/ufo3/glyphs/glif/

A `.glif` file contains:
- name
- unicode codepoints (in hex)
- advance width (or height)
- outline as a series of contours/components
- anchors
- lib and note: tool-specific metadata

points are stored one point per XML element.
It's not efficient, but it is easy.

The description in the UFO spec of a `qcurve` point is not
exactly clear, but it seems that implicit points
(as in TrueType) are intended.

The _name_ field may be changing:
UFO 3 recommends ignoring _name_ in favour of `contents.plist`,
but that may be reversed in UFO 4 in favour of having tools scan
the entire directory for the glyphs in that layer.
This is because the `contents.plist` file is a common source of
conflicts when merging.

The _unicode_ field may be changing, in favour of a `cmap`-like
list that maps from unicode to glyph.
Or the pendulum may be swinging back to using the _unicode_ field.

Using _anchors_ for ligature caret positioning is explicitly
mentioned in the spec.
Presumably they are also used to base-to-mark and mark-to-mark
positioning, and possibly also for kerning?


## Points

A _point_ obviously has an `x` and a `y` coordinate.
It also has a _type_ and a _smooth_ attribute.

The `move` type is only used to describe open contours and is
only used as the first point.
Such contours are unlikely to become geometry in the common
targets.

The remaining types are:

- line
- qcurve (a quadratic beziér)
- curve (a cubic, or redundantly a quadractic or a line)
- offcurve

`line`, `qcurve`, `curve` can all have the `smooth` attribute,
implying a smooth curve (tangent in same as tangent out).


## Components

Components (as in TrueType) are modelled as a UFO `component`
child of the `outline` element.

A 6-element matrix specifies the linear transformation
(although using somewhat unconventional names).


## Unicode

In a binary OpenType file the map between Unicode codepoint and
a glyph is the `cmap`.
It maps from codepoint (Unicode, or historically some other
encoding) to a glyph index.

Of note: a glyph may not be associated with a Unicode codepoint
(it may be printable via GSUB rules, or it may be a stray;
even stray glyphs can be printed by some tools though).
A glyph may be associated with several Unicode codepoints:
nothing prevents the cmap mapping multiple Unicode codepoints
to the same Glyph index (for example, an upper-case only font
may use the same glyph indexes for both upper- and lower-case).


## Metrics, Sidebearings, and so on

In UFO the metrics for a glyph are stored in the `.glif`
representation for that glyph.
So all the data for a single glyph is in one place.

The `.glif` format allows for up to one of each of
a horizontal advance (width) and a vertical advance (height).

There is no explicit mention of horizontal offsetting,
it is implicitly assumes that the left-side is 0.
Unlike TrueType which has an extra degree of freedom in
allowing left-sidebearing and xMin to be different.

There is provision for vertical offsetting in the sense that
for vertical layout it is recommended that the verticalOrigin is
stored in the `public.verticalOrigin` key of the `lib`
dictionary extension mechanism.

In a binary OpenType font file the horizontal metric information is
stored in the `hmtx` table.
This table must be compiled by examining all the glyphs.


## GDEF

OpenType GDEF categories becomes `public.openTypeCategories` in
the UFO's optional `lib.plist` file https://unifiedfontobject.org/versions/ufo3/lib.plist/


## PostScript names

names in `post` (or `CFF`) translate to `public.postscriptNames`.
Or perhaps not, the data is optional.
If `public.postscriptNames` is not present the the glyph names are used.

So it is sufficient to translate names in TTF file to UFO Glyph Names.


## Layers

For now put all TTF glyphs in a single layer.

Note sure i've seen a layered OTF font, but they apparently exist.
Will need to create and map layers.


## Hints

Provision is made in the spec for storing hints in the form of
both TrueType instructions and CFF/PostScript stem hints
(probably not both?).

This uses the `.glif` format general `lib` extension mechanism:
Each `.glif` file can have a `lib` element that is an arbitrary
XML Property List (a key-value dictionary).


# END
