# Common Outline Font Format

Common Outline Font Format: coff for short.

coff grew out of [`glox`](https://git.sr.ht/~drj/glox) by
considering its relationship to
[`tx` from AFDKO](http://adobe-type-tools.github.io/afdko/AFDKO-Overview.html#tx):

The narrow view of `glox` is that it extracts glyph outlines and
converts them to SVG, a reasonably widespread and viewable format.

The broader view of `glox` is that it is a _glyph exchange_
that can convert from and to many formats, using a
_common outline font format_.
The _exchange_ part of this name comes from the `tx` tool,
short for _type exchange_.
`tx` is in the AFDKO toolkit and does many to many format
conversions using ABF (Abstract Font Format) as an intermediate.

There are some crude notes on the [UFO format](ufo.md),
and some thoughts [about CFF](cff.md).

coff is also a Go package.


## sources and targets

A _source_ is something that can be used to generate coff (by
extracting it from a font format for example).

A _target_ is something that coff be converted to.

- SVG, a target (`glox`) and a source (limited, `ttf8`)
- TTF, a source (`glox`) and a target (limited, `ttf8`)
- OTF, a source (`glox`)

- UFO, not implemented; priority source and target
- PDF, not implemented; useful target (following `tx`)

### SVG

SVG is a large spec, too large to be implemented in its
entirety.

An SVG target should be sufficient to reasonably display all
glyphs for the purposes of debugging and proofing.
A secondary goal would be to support downstream tools
(such as kerning previewers or hinters).
One idea is to match the structure of the SVG output of
`hb-view --output-format=svg`, so
that `hb-view` can be used to prototype tests and downstream tools.

An SVG source is not required to be completely general.
Currently there are 2 important SVG sources:
one, the `vec8` tool that produces exact traces of PNG files;
two, the hand-made SVG documents that are the source art for the
font `Ranalina`.

Adding the SVG output of `hb-view` as an SVG source would be useful;
there are two immediates uses:
one, a second pathway for
interpreting TTF/OTF font files;
two, realising a particular
shaping/rendering of a font (optical adjustments, hinting,
variation space instantiation).

Another extension for SVG would be to support conversion of
SVG sources to multi-coloured glyphs, essentially adding support
for the OpenType `COLR` and `CPAL` tables.

Also note that OpenType also has the `SVG` table for
incorporating SVG as-is.
It's not clear if it would be useful to do such a conversion
using coff.
Probably not as coff is used to represent a fairly restricted
notion of outline and
the purpose of the OpenType `SVG` table is
to support SVG in all its richness.

### SVG and ttf8

The current primary consumer of SVG is `ttf8`.
Here i outline the steps that `ttf8` currently uses.

This informs the coff glyph model, which currently doesn't exist.
But `ttf8` has one.

- One SVG group is one glyph
- every path has a class. This class may be empty. The class is
  either the SVG/XML class or derived from the SVG/XML id.
- class is used to sort path elements into baseline, anchors
  (for both base-to-mark positioning and kerning), and contours
  (empty class).
- baseline is used to define a translate transform that brings
  bottom left of baseline to [0 0]
- baseline is used to set the _sides_
- the _glyph em_ (a global default that can be overridden on a
  per-glyph basis; and is for the builtin `.notdef`) and
  a `upem` (a single value for the target) define a
  scale transform.
- paths are converted to coff contours then transformed
- coff contours are grid fitted by `coff.ContoursTTAdjust()`
- kerning pairs and mark/base anchors are computed.

SVG path elements are converted to COFF contours early on,
then geometry data is extracted, contours are transformed,
and glyph position data is compiled.

Notes:

- the paths have an order in the SVG which is preserved, but
  is not relevant to the processing. This seems like a good
  thing, because the same is approximately true for OTF and TTF
  and is relevant to Variable Font processing.
  The order should not be whimsically changed.
- path to contour correspondence is destroyed.
  In SVG one `path` can have multiple contours, and a group can
  have multiple `path` elements. When converting to coff,
  a simple linear list of contours is produced. (this affects
  the visual semantics. In SVG, paths are complete objects that
  are composed by overprinting, thus a contour in one path
  cannot make a hole in another path.
- the path to contour (non-) correspondence means that a single
  contour is not (necessarily) associated with a single SVG id.
  One SVG path has one id but can have several contour.
- notionally anchors are single points, but for convenience of
  drawing in SVG (in Inkscape) they appear in the SVG as lines.
  The bottom left position is used.


## Realisation

`outline.go` in this repo
is a partial realisation of this idea.

The primitive, `Contour`, consists of a sequence of points,
with each point having a field, `OnCurve`, that
specifies whether the point is an on-curve point or not.

The interpretation of `Contour` is variable.

A _normalised_ contour has every point represented.
Line segments will therefore consist of two adjacent on-curve
points with no points in between.
A quadratic curve will have exactly one off-curve points between
two on-curve points.
A cubic curve will have exactly two off-curve points between two
on-curve points.

In principle higher-order Beziér curves can be represented by
having more off-curve control points between on-curve points,
but this is not supported (by the functions that manipulate
`Contour`).

Sources such as TrueType may, as an intermediate, use `Contour`
in non-normal form.
For example implicit on-curve points may be omitted from the
`Contour`.
Using such a `Contour` with normal functions is ambiguous, so
such contours should always be normalised first
(which will be a source-specific operation).

# END
