package coff

// An outline model.
// Represents glyphs that came from either TT or CFF.

import (
	"bytes"
	"fmt"
	"golang.org/x/image/math/f64"
	"image"
	"log"
	"math"
)

// Model a glyph in outline format.
// An Outline is _simple_ when components is empty.
// An Outline is _composite_ when components is non-empty.
// A composite Outline has no contours
// (at least when sourced from TrueType).
// A simple Outline _may_ have no contours, and
// this is usual for the space glyphs.
type Outline struct {
	Components []Component
	Contours   []Contour
}

func (o Outline) String() string {
	buf := &bytes.Buffer{}
	fmt.Fprintln(buf, "Outline")
	fmt.Fprintf(buf, "  Components [%d]\n", len(o.Components))
	fmt.Fprintln(buf, o.Components)
	fmt.Fprintf(buf, "  Contours [%d]\n", len(o.Contours))
	for _, c := range o.Contours {
		fmt.Fprintln(buf, c)
	}

	return string(buf.Bytes())
}

// Models a single contour in a glyph.
type Contour struct {
	P []ContourPoint
	// In UFO a contour can have a unique identifier;
	// in COFF a name.
	Name string
}

// Although in TrueType all points are given in integer coordinates,
// implicit on-curve points are exactly halfway between their
// neighbours, introducing the possibility that those
// points can have fractional (.5) coordinates.
// Which is why we use float.
// Also, CFF support 16.16 fixed point (a slightly obscure fact).
type ContourPoint struct {
	X, Y    float64
	OnCurve bool
}

// A component of a composite glyph.
// This is a model of a _reference_ to a component.
// The outlines of the component have to be fetched separately.
type Component struct {
	// Both GlyphIndex and Base are ways to refer to
	// the component, but only one should be used.
	// When building from SVG, Base is used first (glyphs
	// have no glyph index number), then compiled as a number.
	// When building from TTF or similar, only GlyphIndex is used.
	GlyphIndex int
	Base       string
	// The affine transformation.
	// By (obscure) convention Aff3 is a 3x3 matrix in
	// row-major order, where the bottom [0 0 1] row is implicit.
	M f64.Aff3
}

// a 2D point in floating point coordinates.
type Point struct {
	X, Y float64
}

// Close each contour by ensuring that the end point is
// the same as the start point.
// The outline is both mutated and returned.
func (o *Outline) Close() *Outline {
	for i, contour := range o.Contours {
		N := len(contour.P)
		if contour.P[0] == contour.P[N-1] {
			continue
		}
		o.Contours[i].P = append(contour.P, contour.P[0])
	}
	return o
}

// Convert to an image.Point,
// fatal if the conversion cannot be made exactly.
// :todo: change to propagate error to caller.
func PointToImageExact(p Point) image.Point {
	// Without this check, Inf would pass right through the
	// rest of this function.
	// The explicit check for Inf handles the
	// ContoursBounds() case for empty contours.
	if math.IsInf(p.X, 0) {
		p.X = 0
	}
	if math.IsInf(p.Y, 0) {
		p.Y = 0
	}

	if math.Round(p.X) != p.X {
		log.Fatalf("Cannot convert X of %v to image.Point exactly", p)
	}
	if math.Round(p.Y) != p.Y {
		log.Fatalf("Cannot convert Y of %v to image.Point exactly", p)
	}
	// :todo: fatal error for out of range
	// recall range of 64-bit int:
	// -9223372036854775808 to 9223372036854775807
	// Without the range check this function will return; in the
	// words of the Go Spec "the conversion succeeds but the
	// result value is implementation-dependent".
	return image.Pt(int(p.X), int(p.Y))
}

// Convert to an image.Point, rounding using floor
// when an exact conversion cannot be made.
// This and the following PointToImageCeil are intended for
// min/max bounds conversions.
func PointToImageFloor(p Point) image.Point {
	// Without this check, Inf would pass right through the
	// rest of this function.
	// The explicit check for Inf handles the
	// ContoursBounds() case for empty contours.
	if math.IsInf(p.X, 0) {
		p.X = 0
	}
	if math.IsInf(p.Y, 0) {
		p.Y = 0
	}

	// :todo: fatal error for out of range
	// recall range of 64-bit int:
	// -9223372036854775808 to 9223372036854775807
	// Without the range check this function will return; in the
	// words of the Go Spec "the conversion succeeds but the
	// result value is implementation-dependent".
	return image.Pt(int(math.Floor(p.X)), int(math.Floor(p.Y)))
}

// Convert to an image.Point, rounding using ceil.
func PointToImageCeil(p Point) image.Point {
	// Without this check, Inf would pass right through the
	// rest of this function.
	// The explicit check for Inf handles the
	// ContoursBounds() case for empty contours.
	if math.IsInf(p.X, 0) {
		p.X = 0
	}
	if math.IsInf(p.Y, 0) {
		p.Y = 0
	}

	// :todo: fatal error for out of range
	// recall range of 64-bit int:
	// -9223372036854775808 to 9223372036854775807
	// Without the range check this function will return; in the
	// words of the Go Spec "the conversion succeeds but the
	// result value is implementation-dependent".
	return image.Pt(int(math.Ceil(p.X)), int(math.Ceil(p.Y)))
}

// Somewhat crude bounds.
// It includes the off-curve control points in the bounds,
// making it possibly conservative.
// Though for the conventional contour descriptions for fonts
// it will make no difference, because such descriptions
// routinely include the cardinal extremal points.
func (o *Outline) Bounds() image.Rectangle {
	min, max := ContoursBounds(o.Contours)
	return IntegerBounds(min, max)
}

// Return bounds as (floating point) min (bottom left) and
// max (top right) points.
func ContoursBounds(contours []Contour) (min, max Point) {
	maxX := math.Inf(-99)
	maxY := maxX
	minX := math.Inf(+99)
	minY := minX
	for _, c := range contours {
		for _, p := range c.P {
			minX = math.Min(minX, p.X)
			maxX = math.Max(maxX, p.X)
			minY = math.Min(minY, p.Y)
			maxY = math.Max(maxY, p.Y)
		}

	}
	return Point{minX, minY}, Point{maxX, maxY}
}

func (o *Outline) IsSimple() bool {
	return len(o.Components) == 0
}

func MakeComponentAtXY(x, y float64, base string) Component {
	t := MakeTranslate(x, y)
	return Component{Base: base, M: t}
}

func (c *Component) TransformLeftMultiply(factor f64.Aff3) *Component {
	c.M = TransformMultiply(factor, c.M)
	return c
}

// Return a matrix for a translation.
func MakeTranslate(tx, ty float64) f64.Aff3 {
	return f64.Aff3{
		1, 0, tx,
		0, 1, ty,
	}
}

// Return a matric for a scale (in x and y).
func MakeScale(sx, sy float64) f64.Aff3 {
	return f64.Aff3{
		sx, 0, 0,
		0, sy, 0,
	}
}

// Return the result L×R.
// When the result product is applied to a column-vector point,
// the effect is first applying R then L.
func TransformMultiply(L, R f64.Aff3) f64.Aff3 {
	// We are computing:
	// [ L0 L1 L2   [ R0 R1 R2
	//   L3 L4 L5  ×  R3 R4 R5
	//   0  0  1 ]    0  0  1 ]
	return f64.Aff3{
		L[0]*R[0] + L[1]*R[3], L[0]*R[1] + L[1]*R[4], L[0]*R[2] + L[1]*R[5] + L[2],
		L[3]*R[0] + L[4]*R[3], L[3]*R[1] + L[4]*R[4], L[3]*R[2] + L[4]*R[5] + L[5],
	}
}

// Return the result U where U A == identify
// Mostly following https://math.stackexchange.com/questions/397111/how-to-find-3-x-3-matrix-inverses
func TransformInverse(A f64.Aff3) f64.Aff3 {
	// Picture of A
	// [ A0 A1 A2
	//   A3 A4 A5
	//   0  0  1 ]

	// determinant, "easy" case, A0*A4 - A1*A3
	detA := A[0]*A[4] - A[1]*A[3]

	// cofactors
	CF11 := A[4]
	CF12 := -A[3]
	// CF13 := 0
	CF21 := -A[1]
	CF22 := A[0]
	// CF23 := 0
	CF31 := A[1]*A[5] - A[2]*A[4]
	CF32 := -A[0]*A[5] + A[2]*A[3]
	CF33 := detA

	// divide by detA
	CF11 /= detA
	CF12 /= detA
	CF21 /= detA
	CF22 /= detA
	CF31 /= detA
	CF32 /= detA
	CF33 /= detA

	// transpose

	return f64.Aff3{
		CF11, CF21, CF31,
		CF12, CF22, CF32,
	}
}

// The arguments are TrueType specific.
func (component *Component) SetMatrix(flags uint16, arg1, arg2 int, scale []int16) {
	// ARGS_ARE_XY_VALUES
	if 0 == flags&0x2 {
		log.Fatal("ARGS_ARE_XY_VALUES is 0; NOT IMPLEMENTED\n", component, arg1, arg2)
	}

	x := float64(arg1)
	y := float64(arg2)

	switch len(scale) {
	case 0:
		// No scale, translation only
		component.M = f64.Aff3{
			1, 0, x,
			0, 1, y}
	case 1:
		// uniform scale
		s := scale[0]
		// Convert from F2Dot14 to Float64, by division by 2**14.
		sf := float64(s) / 16384
		component.M = f64.Aff3{
			sf, 0, x,
			0, sf, y}
	default:
		log.Fatal("Component has a non-uniform scale or matrix; NOT IMPLEMENTED\n", component, scale)
	}
}

// Transform a single point
func (component *Component) Transform(p Point) Point {
	return PointTransform(p, component.M)
}

// Transform a single point
func PointTransform(p Point, m f64.Aff3) Point {
	x := p.X*m[0] + p.Y*m[1] + m[2]
	y := p.X*m[3] + p.Y*m[4] + m[5]
	return Point{x, y}
}

// Transform a bounding rectangle (by the component's transform).
func (component *Component) TransformBounds(b image.Rectangle) image.Rectangle {
	// The four corners of the rectangle
	c00 := Point{float64(b.Min.X), float64(b.Min.Y)}
	c01 := Point{float64(b.Min.X), float64(b.Max.Y)}
	c10 := Point{float64(b.Max.X), float64(b.Min.Y)}
	c11 := Point{float64(b.Max.X), float64(b.Max.Y)}

	// transform the points and compute a min and max
	min := component.Transform(c00)
	max := min
	for _, p := range []Point{c01, c10, c11} {
		t := component.Transform(p)
		min.X = math.Min(min.X, t.X)
		min.Y = math.Min(min.Y, t.Y)
		max.X = math.Max(max.X, t.X)
		max.Y = math.Max(max.Y, t.Y)
	}
	res := IntegerBounds(min, max)
	return res
}

// Flatten an outline by fully resolving it to one with
// only contours (so no components).
// Traverses into all the nested components and applies
// all the appropriate transforms.
// Outliner fetches an Outline given a component.
func (o *Outline) Flatten(oer Outliner) *Outline {
	contours := append([]Contour{}, o.Contours...)

	// depth is the maxmimum component depth
	// ps is the number of points
	for i, component := range o.Components {
		_ = i
		cs := flatten(component, oer)
		contours = append(contours, cs...)
	}
	return &Outline{nil, contours}
}

type Outliner interface {
	GetOutline(i int) *Outline
}

func flatten(component Component, o Outliner) []Contour {
	outline := o.GetOutline(component.GlyphIndex)
	flat := outline.Flatten(o)
	transformed := ContoursTransform(flat.Contours, component.M)
	return transformed
}

// Bound (float64) min and max Point to (integer) image.Rectangle
func IntegerBounds(min, max Point) image.Rectangle {
	imin := image.Pt(int(math.Floor(min.X)), int(math.Floor(min.Y)))
	imax := image.Pt(int(math.Ceil(max.X)), int(math.Ceil(max.Y)))
	return image.Rectangle{imin, imax}
}

// Transform all the points of all the contours in the slice
// by the specified transformation.
// The returned contours are completely fresh.
func ContoursTransform(cs []Contour, m f64.Aff3) []Contour {
	contours := make([]Contour, len(cs))
	for i, c := range cs {
		contours[i] = c
		contours[i].P = nil
		for _, p := range c.P {
			tp := p
			point := PointTransform(Point{p.X, p.Y}, m)
			tp.X = point.X
			tp.Y = point.Y
			contours[i].P = append(contours[i].P, tp)
		}
	}

	return contours
}

// When contours have been sourced from TrueType:
// Expand the contours by adding the implicit on-curve points.
// In TrueType outline fonts, the low-level point representation
// allows a space-saving optimisation where an on-curve point
// can be eliminated when it is exactly between two off-curve
// points.
// The existence of these implicit points can be
// inferred from two-adjacent off-curve points.
// This function adds them back in.
func ExpandImplicitTTContours(cs []Contour) []Contour {
	out := []Contour{}
	for _, contour := range cs {
		ps := []ContourPoint{}
		for i, p := range contour.P {
			ps = append(ps, p)
			// Usually gets overwritten;
			// this is the wraparound case.
			next := contour.P[0]
			if i+1 < len(contour.P) {
				next = contour.P[i+1]
			}
			if !p.OnCurve && !next.OnCurve {
				x := (p.X + next.X) / 2
				y := (p.Y + next.Y) / 2
				ps = append(ps, ContourPoint{x, y, true})
			}
		}
		c := contour
		c.P = ps
		out = append(out, c)
	}
	return out
}

// When contours are intended for a TrueType target:
// Adjust the contours to the integer grid.
// The returned contours are completely fresh.
// :todo: a later version will cleverly compute implicit points
// that may lie on the half-grid.
func ContoursTTAdjust(cs []Contour) []Contour {
	contours := make([]Contour, len(cs))
	for i, c := range cs {
		contours[i] = c
		contours[i].P = nil
		for _, p := range c.P {
			tp := p
			tp.X = math.RoundToEven(p.X)
			tp.Y = math.RoundToEven(p.Y)
			contours[i].P = append(contours[i].P, tp)
		}
	}
	return contours
}
